
/*
	
*/

#include "Platform.hpp"
#include <iostream>
using namespace std;

namespace lubre
{
	Platform::Platform(Scene * scene) 
		: GameObject(scene), isActive(false), goUp(false)
	{
		float scale = scene->pixelsToUnits();

		// platform

		b2BodyDef platformBodyDef;

		platformBodyDef.type = b2_kinematicBody;
		platformBodyDef.position.Set(scene->getSceneWidth() - 600.f * scale, 40.f * scale);

		bodies["platform"] = scene->getPhysicsWorld().CreateBody(&platformBodyDef);

		b2PolygonShape platformShape;

		platformShape.SetAsBox(100.f * scale, 20.f * scale);

		b2FixtureDef platformFixtureDef;

		platformFixtureDef.shape = &platformShape;
		platformFixtureDef.density = 100.f;
		platformFixtureDef.friction = 1.f;
		platformFixtureDef.restitution = 0.f;

		bodies["platform"]->CreateFixture(&platformFixtureDef);

		// sensor de la plataforma cuando est� activa

		b2BodyDef platformSensorBodyDef;

		platformSensorBodyDef.type = b2_staticBody;
		platformSensorBodyDef.position.Set(scene->getSceneWidth() - 650.f * scale, 100.f * scale);

		bodies["platformSensor"] = scene->getPhysicsWorld().CreateBody(&platformSensorBodyDef);
		bodies["platformSensor"]->SetUserData(this);

		b2PolygonShape platformSensorShape;

		platformSensorShape.SetAsBox(30.f * scale, 15.f * scale);

		b2FixtureDef platformSensorFixtureDef;

		platformSensorFixtureDef.shape = &platformSensorShape;
		platformSensorFixtureDef.isSensor = true;

		bodies["platformSensor"]->CreateFixture(&platformSensorFixtureDef);


	}

	void Platform::onCollisionEnter()
	{
		cout << "la plataforma sube" << endl;
		goUp = true;
	}

	void Platform::update(float deltaTime)
	{
		if (!isActive) return;
		if (!goUp) return;

		if (bodies["platform"]->GetPosition().y < 49)
		{
			bodies["platform"]->SetTransform(bodies["platform"]->GetPosition() + b2Vec2(0.f, 10.f * deltaTime), 0);
		}

		/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
		{
			bodies["platform"]->SetTransform(bodies["platform"]->GetPosition() + b2Vec2(0.f, 10.f * deltaTime), 0);
		}
		else
		{
			bodies["platform"]->SetLinearVelocity(b2Vec2(0.f, 0.f));
		}*/
	}
}