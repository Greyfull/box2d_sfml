
/*
	Author: David Rogel Pernas

*/

#include "DemoScene.hpp"
#include "Game.hpp"

using namespace lubre;

int main()
{
	// creamo el juego y lo inicializamos
	Game * game = new Game("fisiks test", 1280, 720);
	game->init();

	return 0;
}