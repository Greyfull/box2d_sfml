
#include "ParticleSystem.hpp"

namespace lubre
{
	ParticleSystem::ParticleSystem(vec2 dir, size_t count, vec2 position, sf::Color color)
		: count(count), position(position)
	{
		// generamos las particula
		generateParticles(dir, count, position, color);
	}

	void ParticleSystem::update(float deltaTime)
	{
		// por cada particula -> actualizamos
		for
		(
			auto iterator = pool.begin(), end = pool.end();
			iterator != end;
			++iterator
		)
		{
			iterator->get()->update(deltaTime);
			iterator->get()->poll();
		}

	}

	void ParticleSystem::render(sf::RenderWindow & window)
	{
		// por cada particula -> dibujamos
		for
		(
			auto iterator = pool.begin(), end = pool.end();
			iterator != end;
			++iterator
		)
		{

			Particle * particle = iterator->get();
			sf::CircleShape circle;

			circle.setFillColor(particle->getColor());
			circle.setPosition(particle->getPosition());
			circle.setRadius(2.f);

			window.draw(circle);
		}
	}

	void ParticleSystem::generateParticles(vec2 dir, size_t count, vec2 position, sf::Color color)
	{
		// reservamos memoria
		pool.reserve(count);

		// a�adimos la cantidad de particulas establecida
		for (int i = 0; i < count; ++i)
		{
			pool.push_back(SharedParticle(new Particle(dir.x, dir.y, position, color)));
		}
	}
}