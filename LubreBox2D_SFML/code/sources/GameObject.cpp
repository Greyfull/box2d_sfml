
/*

*/

#include "GameObject.hpp"

namespace lubre
{
	GameObject::~GameObject()
	{
		// destruimos cada unos de los cuerpo

		for (auto & key : bodies)
		{
			scene->getPhysicsWorld().DestroyBody(key.second);
		}

		// destruimos cada uno de los joints

		for (auto & key : joints)
		{
			scene->getPhysicsWorld().DestroyJoint(key.second);
		}

		// destruimos la escena

		delete scene;
	}

	void GameObject::render(sf::RenderWindow & renderer)
	{
		// tomamos la escala para pasar de box2D a SFML
		float scale = scene->unitsToPixels();

		// por cada cuerpo en la lista de cuerpos
		for (auto bodyIt = bodies.begin(), end = bodies.end(); bodyIt != end; ++bodyIt)
		{
			// obtenemos el transform
			b2Transform bodyTransform = bodyIt->second->GetTransform();

			// por cada fixture
			for (auto fixture = bodyIt->second->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
			{
				// obtenemos el tipo de shape
				b2Shape::Type shapeType = fixture->GetShape()->GetType();
				
				if (shapeType == b2Shape::e_circle)
				{
					// casteamos la shape al circulo
					b2CircleShape * circle = static_cast<b2CircleShape *>(fixture->GetShape());

					// obtenemos el radio
					float radius = circle->m_radius * scale;
					// obtenemos la posicion
					b2Vec2 center = circle->m_p;
					// creamos una shape de SFML
					sf::CircleShape shape;

					// le damos una posicion
					shape.setPosition
					(
						// pasamos de box2D a SFML
						box2d_position_to_sfml_position
						(
							// multiplicamos el transform del cuerpo por su posicion
							b2Mul(bodyTransform, center),
							(float)scene->getSceneHeight()
						)
						- sf::Vector2f(radius, radius)
					);

					// le damos un color
					shape.setFillColor(sf::Color::Red);
					// le damos un radio
					shape.setRadius(radius);

					// lo dibujamos
					renderer.draw(shape);
				}
				else if (shapeType == b2Shape::e_polygon)
				{
					// casteamos la shape al poligono
					b2PolygonShape * polygon = static_cast<b2PolygonShape *>(fixture->GetShape());

					// creamos una shape de SFML
					sf::ConvexShape shape;

					// obtenemos el numero de vertices
					int numOfVertices = polygon->GetVertexCount();

					// le pasamos el numero de vertices a la shape
					shape.setPointCount(numOfVertices);

					// modficamos el color si es un sensor
					if (fixture->IsSensor())
					{
						sf::Color color;
						color.r = 0;
						color.g = 255;
						color.b = 0;
						color.a = 100;
						shape.setFillColor(color);
					}
					else
					{
						shape.setFillColor(sf::Color::Green);
					}

					// por cada numero de vertices
					for (int i = 0; i < numOfVertices; ++i)
					{
						// seteamos cada unos de ellos
						shape.setPoint
						(
							i,
							box2d_position_to_sfml_position(b2Mul(bodyTransform, polygon->GetVertex(i)), (float)scene->getSceneHeight())
						);
					}

					// dibujamo la shape
					renderer.draw(shape);
				}
				else if (shapeType == b2Shape::e_edge)
				{
					// casteamos la shape a un edge
					b2EdgeShape * edge = dynamic_cast<b2EdgeShape*>(fixture->GetShape());

					// obtenemos los puntos de la edge
					b2Vec2 point1 = b2Mul(bodyTransform, edge->m_vertex1);
					b2Vec2 point2 = b2Mul(bodyTransform, edge->m_vertex2);

					// los guardamos en un array de vertices de SFML
					sf::Vertex line[] =
					{
						sf::Vertex(box2d_position_to_sfml_position(point1, scene->getSceneHeight()), sf::Color::Green),
						sf::Vertex(box2d_position_to_sfml_position(point2, scene->getSceneHeight()), sf::Color::Green)
					};

					// dibujamos la linea
					renderer.draw(line, 2, sf::Lines);
				}
			}
		}
	}
}