
#include "Floor.hpp"
#include <iostream>
using namespace std;

namespace lubre
{
	Floor::Floor(Scene * scene) : GameObject(scene)
	{
		float scale = scene->pixelsToUnits();
		
		// segmento 1

		b2BodyDef floor01BodyDef;

		floor01BodyDef.type = b2_staticBody;
		floor01BodyDef.position.Set(0, 200.f * scale);
		floor01BodyDef.angle = 0.f;

		bodies["floor01"] = scene->getPhysicsWorld().CreateBody(&floor01BodyDef);
				
		b2EdgeShape floor01Shape;

		floor01Shape.Set( b2Vec2(0.f, 0.f), b2Vec2(200.f * scale, 0.f));

		b2FixtureDef floor01FixtureDef;

		floor01FixtureDef.shape = &floor01Shape;
		floor01FixtureDef.friction = 0.1f;
		floor01FixtureDef.restitution = 0;

		bodies["floor01"]->CreateFixture(&floor01FixtureDef);

		// segmento 2

		b2BodyDef floor02BodyDef;

		floor02BodyDef.type = b2_staticBody;
		floor02BodyDef.position.Set(200.f * scale, 200.f * scale);
		floor02BodyDef.angle = 0.f;

		bodies["floor02"] = scene->getPhysicsWorld().CreateBody(&floor02BodyDef);

		b2EdgeShape floor02Shape;

		floor02Shape.Set(b2Vec2(0.f, 0.f), b2Vec2(100.f * scale, -50.f * scale));

		b2FixtureDef floor02FixtureDef;

		floor02FixtureDef.shape = &floor02Shape;
		floor02FixtureDef.friction = 0.1f;
		floor02FixtureDef.restitution = 0;

		bodies["floor02"]->CreateFixture(&floor02FixtureDef);

		// segmento 3

		b2BodyDef floor03BodyDef;

		floor03BodyDef.type = b2_staticBody;
		floor03BodyDef.position.Set(300.f * scale, 150.f * scale);
		floor03BodyDef.angle = 0.f;

		bodies["floor03"] = scene->getPhysicsWorld().CreateBody(&floor03BodyDef);

		b2EdgeShape floor03Shape;

		floor03Shape.Set(b2Vec2(0.f, 0.f), b2Vec2(100.f * scale, -25.f * scale));

		b2FixtureDef floor03FixtureDef;

		floor03FixtureDef.shape = &floor03Shape;
		floor03FixtureDef.friction = 0.1f;
		floor03FixtureDef.restitution = 0;

		bodies["floor03"]->CreateFixture(&floor03FixtureDef);

		// segmento 4

		b2BodyDef floor04BodyDef;

		floor04BodyDef.type = b2_staticBody;
		floor04BodyDef.position.Set(400.f * scale, 125.f * scale);
		floor04BodyDef.angle = 0.f;

		bodies["floor04"] = scene->getPhysicsWorld().CreateBody(&floor04BodyDef);

		b2EdgeShape floor04Shape;

		floor04Shape.Set(b2Vec2(0.f, 0.f), b2Vec2(50.f * scale, 0.f));

		b2FixtureDef floor04FixtureDef;

		floor04FixtureDef.shape = &floor04Shape;
		floor04FixtureDef.friction = 0.1f;
		floor04FixtureDef.restitution = 0;

		bodies["floor04"]->CreateFixture(&floor04FixtureDef);

		// segmento 5

		b2BodyDef floor05BodyDef;

		floor05BodyDef.type = b2_staticBody;
		floor05BodyDef.position.Set(450.f * scale, 125.f * scale);
		floor05BodyDef.angle = 0.f;

		bodies["floor05"] = scene->getPhysicsWorld().CreateBody(&floor05BodyDef);

		b2EdgeShape floor05Shape;

		floor05Shape.Set(b2Vec2(0.f, 0.f), b2Vec2(100.f * scale, 25.f * scale));

		b2FixtureDef floor05FixtureDef;

		floor05FixtureDef.shape = &floor05Shape;
		floor05FixtureDef.friction = 0.1f;
		floor05FixtureDef.restitution = 0;

		bodies["floor05"]->CreateFixture(&floor05FixtureDef);

		// Zona de carga

		b2BodyDef zonaCargaBodyDef;

		zonaCargaBodyDef.type = b2_staticBody;
		zonaCargaBodyDef.position.SetZero();

		bodies["zonaCarga"] = scene->getPhysicsWorld().CreateBody(&zonaCargaBodyDef);

		b2PolygonShape zonaCargaShape;

		const b2Vec2 points[]
		{
			b2Vec2(scene->getSceneWidth() - 500.f * scale, 40.f * scale),
			b2Vec2(scene->getSceneWidth(), 40.f * scale),
			b2Vec2(scene->getSceneWidth(), 90.f * scale),
			b2Vec2(scene->getSceneWidth() - 500.f * scale, 65.f * scale)
		};

		zonaCargaShape.Set(points, 4);

		b2FixtureDef zonaCargaFixtureDef;

		zonaCargaFixtureDef.shape = &zonaCargaShape;
		zonaCargaFixtureDef.friction = 1.f;
		zonaCargaFixtureDef.restitution = 0.f;

		bodies["zonaCarga"]->CreateFixture(&zonaCargaFixtureDef);

		// LIMITES

		// right

		b2BodyDef rightLimitBodyDef;

		rightLimitBodyDef.type = b2_staticBody;
		rightLimitBodyDef.position.Set(scene->getSceneWidth() + 1.f * scale, 0.f);

		bodies["rightLimit"] = scene->getPhysicsWorld().CreateBody(&rightLimitBodyDef);

		b2EdgeShape rightLimitShape;

		rightLimitShape.Set(b2Vec2(0.f, 0.f), b2Vec2(0.f, scene->getSceneHeight()));

		b2FixtureDef rightLimitFixtureDef;

		rightLimitFixtureDef.shape = &rightLimitShape;
		rightLimitFixtureDef.friction = 1.f;
		rightLimitFixtureDef.restitution = 0.f;

		bodies["rightLimit"]->CreateFixture(&rightLimitFixtureDef);

		// left

		b2BodyDef leftLimitBodyDef;

		leftLimitBodyDef.type = b2_staticBody;
		leftLimitBodyDef.position.Set(0.f, 0.f);

		bodies["leftLimit"] = scene->getPhysicsWorld().CreateBody(&leftLimitBodyDef);

		b2EdgeShape leftLimitShape;

		leftLimitShape.Set(b2Vec2(0.f, 0.f), b2Vec2(0.f, scene->getSceneHeight()));

		b2FixtureDef leftLimitFixtureDef;

		leftLimitFixtureDef.shape = &leftLimitShape;
		leftLimitFixtureDef.friction = 1.f;
		leftLimitFixtureDef.restitution = 0.f;

		bodies["leftLimit"]->CreateFixture(&leftLimitFixtureDef);

	}

	void Floor::onCollisionEnter()
	{

	}


	void Floor::update(float deltaTime)
	{
		
	}
}