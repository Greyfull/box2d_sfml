
/*
	
*/

#include "Particle.hpp"

#include <iostream>
using namespace std;

namespace lubre
{
	Particle::Particle(float dirX, float dirY, vec2 position, sf::Color color)
		: position(position), color(color), tick(0)
	{
		// le damos un color random a la particula
		this->color = sf::Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1, 255);

		// le damos una velocidad random a la particula por su direccion
		velocity.x = (rand() % 300) * dirX;
		velocity.y = (rand() % 300) * dirY;

		// le damos un alfa a la particula
		alphaSub = this->color.a / (rand() % 50 + 5);
	}

	void Particle::update(float deltaTime)
	{
		// actualizamos la posicion de la particula
		position += velocity * deltaTime;
	}

	bool Particle::poll()
	{		
		if (color.a - alphaSub <= 0 || tick > 50)
		{
			return false;
		}

		// restamos alfa del color
		color.a -= alphaSub;

		// capamos el alfa 
		if (color.a > 200)
		{
			color.a = 200;
		}

		// incrementamos el tick
		tick++;

		return true;
	}
}