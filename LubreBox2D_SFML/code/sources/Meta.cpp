/*
*/

#include "Meta.hpp"

#include <iostream>
using namespace std;

namespace lubre
{
	Meta::Meta(Scene * scene) 
		: GameObject(scene), isFinish(false)
	{
		float scale = scene->pixelsToUnits();
		
		// meta

		b2BodyDef metaBodyDef;

		metaBodyDef.type = b2_staticBody;
		metaBodyDef.position.Set(475.f * scale, 500.f * scale);

		bodies["meta"] = scene->getPhysicsWorld().CreateBody(&metaBodyDef);

		b2PolygonShape metaShape;

		metaShape.SetAsBox(100.f * scale, 10.f * scale);

		b2FixtureDef metaFixtureDef;

		metaFixtureDef.shape = &metaShape;
		metaFixtureDef.friction = 1.f;
		metaFixtureDef.restitution = 0.f;

		bodies["meta"]->CreateFixture(&metaFixtureDef);

		// deposito

		b2BodyDef dropVBodyDef;

		dropVBodyDef.type = b2_staticBody;

		bodies["dropV"] = scene->getPhysicsWorld().CreateBody(&dropVBodyDef);

		b2PolygonShape dropVShape;

		dropVShape.SetAsBox(70.f * scale, 10.f * scale, b2Vec2(335.f * scale, 455.f * scale), 45.f * DEGTORAD);

		b2FixtureDef dropVFixtureDef;

		dropVFixtureDef.shape = &dropVShape;

		bodies["dropV"]->CreateFixture(&dropVFixtureDef);

		dropVShape.SetAsBox(70.f * scale, 10.f * scale, b2Vec2(245.f * scale, 455.f * scale), -45.f * DEGTORAD);

		bodies["dropV"]->CreateFixture(&dropVFixtureDef);

		// drop zone sensor

		b2BodyDef dropZoneSensorBodyDef;

		dropZoneSensorBodyDef.type = b2_staticBody;

		bodies["dropZoneSensor"] = scene->getPhysicsWorld().CreateBody(&dropZoneSensorBodyDef);
		bodies["dropZoneSensor"]->SetUserData(this);

		b2PolygonShape dropZoneSensorShape;

		const b2Vec2 points[] =
		{
			b2Vec2(245.f * scale, 500.f * scale),
			b2Vec2(295.f * scale, 455.f * scale),
			b2Vec2(335.f * scale, 500.f * scale)
		};

		dropZoneSensorShape.Set(points, 3);

		b2FixtureDef dropZoneSensorFixtureDef;

		dropZoneSensorFixtureDef.shape = &dropZoneSensorShape;
		dropZoneSensorFixtureDef.isSensor = true;
		dropZoneSensorFixtureDef.friction = 1.f;

		bodies["dropZoneSensor"]->CreateFixture(&dropZoneSensorFixtureDef);		
	}

	void Meta::onCollisionEnter()
	{
		isFinish = true;
	}

	void Meta::update(float deltaTime)
	{
		if (!isFinish) return;

		scene->addParticleSystem(sf::Vector2f(555.f, 200.f), sf::Vector2f(1.f, -1.f));
		scene->addParticleSystem(sf::Vector2f(400.f, 200.f), sf::Vector2f(-1.f, -1.f));
		
		isFinish = false;
	}
}