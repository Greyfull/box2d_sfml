/*
	Author: David Rogel Pernas
	Date: 1/12/2018
	Info:
*/

#include "Vehicle.hpp"
#include <iostream>
using namespace std;

namespace lubre
{
	Vehicle::Vehicle(Scene * scene) : GameObject(scene)
	{
		float scale = scene->pixelsToUnits();

		// CAR

		b2BodyDef carBodyDef;

		carBodyDef.type = b2_dynamicBody;
		carBodyDef.position.Set(100.f * scale, 250.f * scale);			
		
		bodies["car"] = scene->getPhysicsWorld().CreateBody(&carBodyDef);
		bodies["car"]->SetUserData(this);

		b2PolygonShape carShape;
		carShape.SetAsBox(30.f * scale, 15.f * scale);

		b2FixtureDef carFixtureDef;
		carFixtureDef.shape = &carShape;

		carFixtureDef.density = 1000.f;
		carFixtureDef.friction = 1.f;
		carFixtureDef.restitution = 0.f;

		bodies["car"]->CreateFixture(&carFixtureDef);

		// CAJA

		b2BodyDef boxBodyDef;

		boxBodyDef.type = b2_dynamicBody;		
		boxBodyDef.position.Set(bodies["car"]->GetPosition().x - 3.75f * scale, bodies["car"]->GetPosition().y + 20.f * scale);
		
		bodies["carBox"] = scene->getPhysicsWorld().CreateBody(&boxBodyDef);

		b2PolygonShape boxShape;

		boxShape.SetAsBox(30.f * scale, 5.f * scale);

		b2FixtureDef boxFixtureDef;

		boxFixtureDef.shape = &boxShape;
		boxFixtureDef.density = 20.f;
		boxFixtureDef.friction = 1.f;
		boxFixtureDef.restitution = 0.1f;
		
		bodies["carBox"]->CreateFixture(&boxFixtureDef);

		boxShape.SetAsBox(15.f * scale, 5.f * scale, b2Vec2(-25.f * scale, + 20.f * scale), 90 * DEGTORAD);

		bodies["carBox"]->CreateFixture(&boxFixtureDef);

		boxShape.SetAsBox(15.f * scale, 5.f * scale, b2Vec2(+25.f * scale, + 20.f * scale), 90 * DEGTORAD);

		bodies["carBox"]->CreateFixture(&boxFixtureDef);


		// FRONT WHEEL

		b2BodyDef frontWheelBodyDef;

		frontWheelBodyDef.type = b2_dynamicBody;
		frontWheelBodyDef.position.Set(bodies["car"]->GetPosition().x - 10.f * scale, bodies["car"]->GetPosition().y - 1.f * scale);

		bodies["frontWheel"] = scene->getPhysicsWorld().CreateBody(&frontWheelBodyDef);

		b2CircleShape frontWheelShape;
		frontWheelShape.m_radius = 10.f * scale;

		b2FixtureDef frontWheelFixtureDef;
		frontWheelFixtureDef.shape = &frontWheelShape;
		frontWheelFixtureDef.density = 150.f;
		frontWheelFixtureDef.friction = 0.4f;
		frontWheelFixtureDef.restitution = 0.f;

		bodies["frontWheel"]->CreateFixture(&frontWheelFixtureDef);


		// BACK WHEEL

		b2BodyDef backWheelBodyDef;

		backWheelBodyDef.type = b2_dynamicBody;
		backWheelBodyDef.position.Set(bodies["car"]->GetPosition().x + 10.f * scale, bodies["car"]->GetPosition().y - 1.f * scale);

		bodies["backWheel"] = scene->getPhysicsWorld().CreateBody(&backWheelBodyDef);

		b2CircleShape backWheelShape;
		backWheelShape.m_radius = 10.f * scale;
		
		b2FixtureDef backWheelFixtureDef;
		backWheelFixtureDef.shape = &backWheelShape;
		backWheelFixtureDef.density = 150.f;
		backWheelFixtureDef.friction = 0.4f;
		backWheelFixtureDef.restitution = 0.f;


		bodies["backWheel"]->CreateFixture(&backWheelFixtureDef);

		// JOINT FRONT WHEEL

		b2RevoluteJointDef frontWheelRevoluteJointDef;

		frontWheelRevoluteJointDef.bodyA = bodies["car"];
		frontWheelRevoluteJointDef.bodyB = bodies["frontWheel"];
		frontWheelRevoluteJointDef.collideConnected = false;

		frontWheelRevoluteJointDef.localAnchorA.Set(3, -2);
		frontWheelRevoluteJointDef.localAnchorB.Set(0, 0);

		frontWheelRevoluteJointDef.enableMotor = true;
		frontWheelRevoluteJointDef.maxMotorTorque = 100000;

		joints["frontWheelRear"] = scene->getPhysicsWorld().CreateJoint(&frontWheelRevoluteJointDef);
		
		// JOINT BACK WHEEL

		b2RevoluteJointDef backWheelRevoluteJointDef;
		
		backWheelRevoluteJointDef.bodyA = bodies["car"];
		backWheelRevoluteJointDef.bodyB = bodies["backWheel"];
		backWheelRevoluteJointDef.collideConnected = false;

		backWheelRevoluteJointDef.referenceAngle = 0;

		backWheelRevoluteJointDef.localAnchorA.Set(-3, -2);
		backWheelRevoluteJointDef.localAnchorB.Set(0, 0);

		backWheelRevoluteJointDef.enableMotor = true;
		backWheelRevoluteJointDef.maxMotorTorque = 100000;

		joints["backWheelRear"] = scene->getPhysicsWorld().CreateJoint(&backWheelRevoluteJointDef);		

		// JOINT BOX AXIS

		b2RevoluteJointDef boxAxisRevoluteJointDef;

		boxAxisRevoluteJointDef.bodyA = bodies["car"];
		boxAxisRevoluteJointDef.bodyB = bodies["carBox"];
		boxAxisRevoluteJointDef.collideConnected = true;

		boxAxisRevoluteJointDef.localAnchorA.Set(-3.f, 1.5f);
		boxAxisRevoluteJointDef.localAnchorB.Set(-3.f, -1.f);

		boxAxisRevoluteJointDef.enableMotor = true;
		boxAxisRevoluteJointDef.maxMotorTorque = 8500;

		boxAxisRevoluteJointDef.enableLimit = true;
		boxAxisRevoluteJointDef.upperAngle = 90.f * DEGTORAD;
		boxAxisRevoluteJointDef.lowerAngle = 1.f * DEGTORAD;

		joints["boxAxis"] = scene->getPhysicsWorld().CreateJoint(&boxAxisRevoluteJointDef);

		// joint axis point reference

		b2BodyDef pointReferenceBodyDef;

		pointReferenceBodyDef.active = false;
		pointReferenceBodyDef.type = b2_kinematicBody;

		pointReferenceBodyDef.position = joints["boxAxis"]->GetAnchorB();

		bodies["pointReference"] = scene->getPhysicsWorld().CreateBody(&pointReferenceBodyDef);

		b2CircleShape pointReferenceShape;

		pointReferenceShape.m_radius = 5.f * scale;

		b2FixtureDef pointReferenceFixtureDef;
		pointReferenceFixtureDef.shape = &pointReferenceShape;

		bodies["pointReference"]->CreateFixture(&pointReferenceFixtureDef);

	}

	void Vehicle::onCollisionEnter()
	{

	}

	void Vehicle::update(float deltaTime)
	{
		b2RevoluteJoint * front = dynamic_cast<b2RevoluteJoint*>(joints["frontWheelRear"]);
		b2RevoluteJoint * back = dynamic_cast<b2RevoluteJoint*>(joints["backWheelRear"]);
		
		b2RevoluteJoint * box = dynamic_cast<b2RevoluteJoint*>(joints["boxAxis"]);

		bodies["pointReference"]->SetTransform(joints["boxAxis"]->GetAnchorB(), 0.f);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
		{
			box->SetMotorSpeed(10);
		}
		else
		{
			box->SetMotorSpeed(0);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
		{
			front->SetMotorSpeed(8000);
			back->SetMotorSpeed(8000);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
		{
			front->SetMotorSpeed(-8000);
			back->SetMotorSpeed(-8000);
		}
		else
		{
			front->SetMotorSpeed(0);
			back->SetMotorSpeed(0);
		}
	}
}