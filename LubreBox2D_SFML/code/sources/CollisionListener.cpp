/*

*/

#include "CollisionListener.hpp"
#include "GameObject.hpp"

namespace lubre
{
	void CollisionListener::BeginContact(b2Contact * contact)// override;
	{
		//metodo que maneja cuando colisionan dos objetos 

		// obtenemos los dos objeto que colisionan, mediante el contact
		void * fixtureA = contact->GetFixtureA()->GetBody()->GetUserData();
		void * fixtureB = contact->GetFixtureB()->GetBody()->GetUserData();
		
		// si los dos existen y no son iguales
		if ((fixtureA && fixtureB) && (fixtureA != fixtureB))
		{
			// casteamos el primero a un GameObject
			GameObject * goA = static_cast<GameObject*>(fixtureA);

			if (goA)
			{
				// llamamos al manejador de colisiones de ese GameObject
				goA->onCollisionEnter();
			}

			// casteamos el segundo a un GameObject
			GameObject * goB = static_cast<GameObject*>(fixtureB);

			if (goB)
			{
				// llamamos al manejador de colisiones de ese GameObject
				goB->onCollisionEnter();
			}
		}
	}
}