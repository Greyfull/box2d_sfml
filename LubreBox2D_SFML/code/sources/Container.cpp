/*

*/

#include "Container.hpp"
#include "Platform.hpp"

namespace lubre
{
	Container::Container(Scene * scene) 
		: GameObject(scene), dropContent(false)
	{
		// escala para poder manejar bien box2D
		float scale = scene->pixelsToUnits();

		// Pilar

		// definimos el cuerpo fisico
		b2BodyDef pilarBodyDef;

		// lo activamos
		pilarBodyDef.active = true; 
		// le damos el tipo de cuerpo
		pilarBodyDef.type = b2_staticBody; 
		// le damos una posicion
		pilarBodyDef.position.Set(scene->getSceneWidth() - 200.f * scale, 180.f * scale); 
		
		// creamos el cuerpo mediante el mundo fisico de la escena
		bodies["pilar"] = scene->getPhysicsWorld().CreateBody(&pilarBodyDef);

		// creamos la shape del cuerpo
		b2PolygonShape pilarShape;

		// le damos un ancho y un alto
		pilarShape.SetAsBox(10.f * scale, 100.f * scale);

		// creamos la fixture del cuerpo
		b2FixtureDef pilarFixtureDef;

		// le pasamos la shape a la fixture
		pilarFixtureDef.shape = &pilarShape;
		// indicamos que va a ser un sensor
		pilarFixtureDef.isSensor = true;

		// creamos la fixture en el cuerpo fisico
		bodies["pilar"]->CreateFixture(&pilarFixtureDef);


		// Container

		// creamo el cuerpo fisico
		b2BodyDef containerBodyDef;

		// le damos el tipo de cuerpo
		containerBodyDef.type = b2_dynamicBody;
		// le damos una posicion
		containerBodyDef.position.Set(scene->getSceneWidth() - 235.f * scale, 320.f * scale);

		// lo creamos mediante el mundo fisico de la escena
		bodies["container"] = scene->getPhysicsWorld().CreateBody(&containerBodyDef);

		// creamos la shape del cuerpo
		b2PolygonShape containerShape;

		// le damos un ancho y un alto
		containerShape.SetAsBox(20.f * scale, 20.f * scale);

		// creamos la fixture del cuerpo
		b2FixtureDef containerFixtureDef;

		// aplicamos la shape a la fixture
		containerFixtureDef.shape = &containerShape;
		// le damos una densidad
		containerFixtureDef.density = 10.f;

		// a�adimos la fixture al cuerpo
		bodies["container"]->CreateFixture(&containerFixtureDef);

		// ya que este cuerpo est� compuesto de varias parte hay que definirla y a�adirlar

		// definimos una nueva parte, con su ancho, alto, posicion y rotacion
		containerShape.SetAsBox(10.f * scale, 50.f * scale, b2Vec2(-20.f * scale, 35.f * scale), 45 * DEGTORAD);

		// la a�adimos al cuerpo
		bodies["container"]->CreateFixture(&containerFixtureDef);

		containerShape.SetAsBox(10.f * scale, 50.f * scale, b2Vec2(20.f * scale, 35.f * scale), -45 * DEGTORAD);

		bodies["container"]->CreateFixture(&containerFixtureDef);


		// Container Revolute Joint

		// creamos el joint def
		b2RevoluteJointDef containerRevoluteJointDef;

		// le pasamos los cuerpos que van a estar unidos
		containerRevoluteJointDef.bodyA = bodies["pilar"];
		containerRevoluteJointDef.bodyB = bodies["container"];
		// le decimos que no colisionen entre ellos
		containerRevoluteJointDef.collideConnected = false;

		// colocamos los anchor de cada cuerpo
		containerRevoluteJointDef.localAnchorA.Set(0.f, 15.f);
		containerRevoluteJointDef.localAnchorB.Set(0.f, 0.f);

		// activamos el motor para que podamos darle una rotacion fisica al joint
		containerRevoluteJointDef.enableMotor = true;
		// aplicamos una rotacion de torque maxima
		containerRevoluteJointDef.maxMotorTorque = 5000;

		// activamos el limite de rotacion 
		containerRevoluteJointDef.enableLimit = true;
		// el angulo m�s grande
		containerRevoluteJointDef.upperAngle = 5.f * DEGTORAD;
		// el angulo m�s peque�o
		containerRevoluteJointDef.lowerAngle = -120.f * DEGTORAD;

		// cremos el joint mediante el mundo fisico de la escena
		joints["containerAxis"] = scene->getPhysicsWorld().CreateJoint(&containerRevoluteJointDef);

		// content

		// vamos a crear 10 bolas dentro del container

		for (size_t i = 0; i < 10; ++i)
		{
			std::string name = "ball";
			name += std::to_string(i);

			b2BodyDef ballBodyDef;

			ballBodyDef.type = b2_dynamicBody;
			ballBodyDef.position.Set(scene->getSceneWidth() - 240.f * scale, 400.f * scale);

			bodies[name] = scene->getPhysicsWorld().CreateBody(&ballBodyDef);
			bodies[name]->SetUserData(this);

			b2CircleShape ballShape;
			ballShape.m_radius = 5.f * scale;

			b2FixtureDef ballFixtureDef;
			ballFixtureDef.shape = &ballShape;
			ballFixtureDef.density = 1.f;
			ballFixtureDef.friction = 1.f;
			ballFixtureDef.restitution = 0.f;

			bodies[name]->CreateFixture(&ballFixtureDef);
		}

		// sensor content activate

		b2BodyDef sensorContentBodyDef;

		sensorContentBodyDef.type = b2_staticBody;
		sensorContentBodyDef.position.Set(scene->getSceneWidth() - 70.f * scale, 150.f * scale);		
		
		bodies["sensorContent"] = scene->getPhysicsWorld().CreateBody(&sensorContentBodyDef);
		bodies["sensorContent"]->SetUserData(this);

		b2PolygonShape sensorContentShape;

		sensorContentShape.SetAsBox(50.f * scale, 50.f * scale);

		b2FixtureDef sensorContentFixtureDef;

		sensorContentFixtureDef.shape = &sensorContentShape;
		sensorContentFixtureDef.isSensor = true;		

		bodies["sensorContent"]->CreateFixture(&sensorContentFixtureDef);
	}

	void Container::onCollisionEnter()
	{
		// cuando el jugador llega al sensor...

		// activamos que se pueda soltar la carga
		dropContent = true;
		// activamos la plataforma
		static_cast<Platform*>(scene->getGameObject("platform"))->activate();
	}

	void Container::update(float deltaTime)
	{
		// casteamos el revoluteJointDef a revoluteJoint
		b2RevoluteJoint * container = dynamic_cast<b2RevoluteJoint*>(joints["containerAxis"]);

		if (dropContent)
		{
			// aceleramos el joint
			container->SetMotorSpeed(-200);
		}
	}
}