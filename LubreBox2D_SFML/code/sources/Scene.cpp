
#include "Scene.hpp"

namespace lubre
{
	Scene::Scene(b2Vec2 gravity, int w, int h, float scale)
		: physicsWorld(gravity), sceneWidth(w / (int)scale), 
		sceneHeight(h / (int)scale), scale(scale)
	{
		// creamos el mundo su ancho su alto y seteamos la escala
	}

	Scene::~Scene()
	{
		// limpiamos los mapas de objetos y sistemas de particulas

		mapOfGameObjects.clear();
		particleSystems.clear();
	}

	void Scene::addParticleSystem(sf::Vector2f position, sf::Vector2f dir)
	{
		// a�adimos un nuevo sistema de particula
		particleSystems.push_back(SharedParticleSystem(new ParticleSystem(dir, 100, position)));
	}

	void Scene::addGameObject(const String name, SharedGameObject & gameObject)
	{
		// a�adimos un nuevo objeto con un nombre
		mapOfGameObjects[name] = gameObject;
	}

	GameObject * Scene::getGameObject(const String & name)
	{
		// si exite el objeto
		if (mapOfGameObjects.count(name))
		{
			// lo retorna
			return mapOfGameObjects[name].get();
		}
		else
		{
			// si no retorna nullptr
			return nullptr;
		}
	}

	void Scene::update(float deltaTime)
	{
		// actualizamos las fisicas de box2D pasandole: 
		// un deltaTime, la velocidad de las iteraciones 
		// y en cuanto se desglosa una iteracion
		physicsWorld.Step(deltaTime, 8, 4);
	}
}