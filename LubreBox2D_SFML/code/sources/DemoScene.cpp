
/*

*/

#include "DemoScene.hpp"

#include "Vehicle.hpp"
#include "Floor.hpp"
#include "Platform.hpp"
#include "Container.hpp"
#include "Meta.hpp"

#include "CollisionListener.hpp"

namespace lubre
{
	DemoScene::DemoScene(b2Vec2 gravity, int w, int h, float scale)
		: Scene(gravity, w, h, scale)
	{
		// craemos y a�adirmos al mundo el listener de colisiones
		CollisionListener * collisionListener = new CollisionListener;
		physicsWorld.SetContactListener(collisionListener);

		// a�adimos los objetos a la escena 
		addGameObject("vehicle", SharedGameObject(new Vehicle(this)));
		addGameObject("curvedFloor", SharedGameObject(new Floor(this)));
		addGameObject("platform", SharedGameObject(new Platform(this)));
		addGameObject("container", SharedGameObject(new Container(this)));
		addGameObject("meta", SharedGameObject(new Meta(this)));
	}

	void DemoScene::update(float deltaTime)
	{
		// por cada objeto llamamos a su update
		for
		(
			auto iterator = mapOfGameObjects.begin(), end = mapOfGameObjects.end();
			iterator != end;
			++iterator
		)
		{
			auto key = iterator->first;
			mapOfGameObjects[key]->update(deltaTime);
		}

		// actualizamos el update padre donde esta el update del mundo fisico
		Scene::update(deltaTime);

		// actualizamos los sistemas de particulas
		for
		(
			auto iterator = particleSystems.begin(), end = particleSystems.end();
			iterator != end;
			++iterator
		)
		{
			iterator->get()->update(deltaTime);			
		}
	}

	void DemoScene::render(sf::RenderWindow & renderer)
	{
		// pintamos los objetos
		for
		(
			auto iterator = mapOfGameObjects.begin(), end = mapOfGameObjects.end();
			iterator != end;
			++iterator
		)
		{
			auto key = iterator->first;
			mapOfGameObjects[key]->render(renderer);
		}

		// pintamos los sistemas de particulas
		for
		(
			auto iterator = particleSystems.begin(), end = particleSystems.end();
			iterator != end;
			++iterator
		)
		{
			iterator->get()->render(renderer);
		}
	}
}