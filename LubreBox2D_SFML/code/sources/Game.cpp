#include "Game.hpp"
#include "DemoScene.hpp"

using namespace sf;

namespace lubre
{
	Game::Game(const char* title, int width, int height)
		: window(VideoMode(width, height), title, Style::Titlebar | Style::Close, ContextSettings(32)),
		scene(new DemoScene(b2Vec2(0, -10), window.getSize().x, window.getSize().y, 10.f))
	{
		// capamos el frame rate a 60
		window.setFramerateLimit(60);
	}	

	void Game::init()
	{
		// llamamos al gameloop del juego
		gameLoop();
	}

	void Game::gameLoop()
	{
		constexpr unsigned FPS = 60;
		const Time timePerUpdate = seconds(1.0f / float(FPS));

		// inicializamos las variables que manejan el tiempo

		Clock timer;
		auto lastTime = Time::Zero;
		auto lag = Time::Zero;

		auto time = timer.getElapsedTime();
		auto deltaTime = time - lastTime;

		do
		{
			// obtenemos el tiempo actual
			time = timer.getElapsedTime();
			
			// actualizamos el juego pasandole el deltaTime
			update(deltaTime.asSeconds());

			// limpiamos la ventana
			window.clear();

			// renderizamos
			render();

			// cambiamos los buffers
			window.display();

			// manejamos los eventos
			handleEvents();

			// calculamos el deltaTime
			deltaTime = time - lastTime;
			lastTime = time;
		} 
		// todo esto mientras no se cierre la escena
		while (window.isOpen());
	}

	void Game::handleEvents()
	{
		Event e;

		// llamamos al poll events
		while (window.pollEvent(e))
		{
			switch (e.type)
			{
				case Event::Closed:
					// cerramos la ventana si se clica cerrar
					window.close();
				break;
			}
		}
	}

	void Game::update(float deltaTime)
	{
		// actualizamos la escena
		scene->update(deltaTime);
	}

	void Game::render() 
	{
		// pintamos la escena
		scene->render(window);
	}
}