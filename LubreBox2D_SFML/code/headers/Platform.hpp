
/**
* @file Platform.hpp
* @author David Rogel Pernas
* @date 5/2/2018
* @class Platform
* @brief Clase que representa la plataforma elevadora del juego
*/

#ifndef LUBRE_PLATFORM_HEADER
#define LUBRE_PLATFORM_HEADER

#include "GameObject.hpp"

namespace lubre
{
	class Platform : public GameObject
	{
	private:

		/**
		* @brief indica si la plataforma est� activa
		*/
		bool isActive;
		/**
		* @brief indica si la plataforma debe subir
		*/
		bool goUp;

	public:

		/**
		* @brief Constructor que llamar a todos los metodos necesarios para generar la estructura
		* @param scene -> escena padre
		*/
		Platform(Scene * scene);

	public:

		/**
		* @brief metodo que activa la plataforma
		*/
		void activate()
		{
			isActive = true;
		}

	public:

		/**
		* @brief metodo que ejecuta las colisiones
		*/
		void onCollisionEnter() override;

	public:

		/**
		* @brief metodo que actualiza el gameObject
		* @param deltaTime -> tiempo entre frames, se pasa desde la clase Game
		*/
		void update(float deltaTime) override;
	};
}

#endif