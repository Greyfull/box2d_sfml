
/**
* @file GameObject.hpp
* @author David Rogel Pernas
* @date 12/1/2017
* @class GameObject
* @brief Clase base de cada objeto del juego, contiene una map de los bodies y joints
*/

#ifndef LUBRE_GAME_OBJECT_HEADER
#define LUBRE_GAME_OBJECT_HEADER
#include <iostream>
#include "Scene.hpp"

namespace lubre
{
	class GameObject
	{
	protected:

		typedef std::string String;
		typedef std::shared_ptr<sf::Shape> SharedSFShape;

	protected:

		/**
		* @brief puntero a la escena padre
		*/
		Scene * scene;

		/**
		* @brief mapa de cuerpos de box2D
		*/
		std::map<String, b2Body *> bodies;
		/**
		* @brief mapa de joints de box2D
		*/
		std::map<String, b2Joint *> joints;
	public:

		/**
		* @brief Constructor de un GameObject
		* @param scene -> escena padre
		*/
		GameObject(Scene * scene) : scene(scene)
		{
		}

		/**
		* @brief Destruimos el puntero a escena y limpiamos los mapas
		*/
		virtual ~GameObject();

	public:

		/**
		* @brief retorna la escena
		*/
		Scene * getScene()
		{
			return scene;
		}

	public:

		/**
		* @brief metodo a implementar que maneja las colisiones
		*/
		virtual void onCollisionEnter() = 0;

	public:

		/**
		* @brief metodo a implementar que actualiza el estado de cada objeto
		*/
		virtual void update(float deltaTime) = 0;		

		/**
		* @brief metodo que renderiza el objeto
		*/
		void render(sf::RenderWindow & renderer);

	private:

		/**
		* @brief metodo que cambia la posicion de box2D a la de SFML 
		* ya que en box2D el eje Y es positivo hacia arriba y en SFML es positivo hacia abajo
		* este metodo se ha tomado y modificado del repositorio de ejemplos de Animacion 2D -> https://bitbucket.org/angel-esne/c-animation-examples <-
		* @param box2d_position -> posicion de box2D
		* @param window_height -> altura de la ventana
		*/
		inline sf::Vector2f box2d_position_to_sfml_position(const b2Vec2 & box2d_position, float window_height)
		{
			float scale = scene->unitsToPixels();
			return sf::Vector2f(box2d_position.x * scale, (window_height - box2d_position.y) * scale);
		}

	};
}

#endif