
/**
* @file Container.hpp
* @author David Rogel Pernas
* @date 7/2/2017
* @class Container
* @brief Clase que crea el contenedor de bolas
*/

#ifndef LUBRE_CONTAINER_HEADER
#define LUBRE_CONTAINER_HEADER

#include "GameObject.hpp"

namespace lubre
{
	class Container : public GameObject
	{
	private:

		/**
		* @brief cuando se deben lanzar las bolas
		*/
		bool dropContent;

	public:

		/**
		* @brief Constructor que llamar a todos los metodos necesarios para generar la estructura
		* @param scene -> escena padre
		*/
		Container(Scene * scene);

	public:
		
		/**
		* @brief metodo que ejecuta las colisiones
		*/
		void onCollisionEnter() override;
		
	public:

		/**
		* @brief metodo que actualiza el gameObject
		* @param deltaTime -> tiempo entre frames, se pasa desde la clase Game
		*/
		void update(float deltaTime) override;
	};
}

#endif