
/**
* @file Scene.hpp
* @author David Rogel Pernas
* @date 12/1/2018
* @class Scene
* @brief Clase base para las escenas de juego
*/

#ifndef LUBRE_SCENE_HEADER
#define LUBRE_SCENE_HEADER

#include <map>
#include <memory>
#include <string>

#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>

#include "ParticleSystem.hpp"

namespace lubre
{
	/**
	* @brief constante para poder pasar de grados a radianes
	*/
	static const float DEGTORAD = 0.0174533f;

	class GameObject;
	
	class Scene
	{
	protected:

		typedef std::string String;
		typedef std::shared_ptr<GameObject> SharedGameObject;

		typedef std::shared_ptr<ParticleSystem> SharedParticleSystem;

	protected:

		/**
		* @brief mundo fisico de box2d
		*/
		b2World physicsWorld;

		/**
		* @brief mapa de GameObjets
		*/
		std::map<String, SharedGameObject> mapOfGameObjects;
		/**
		* @brief vector de sistemas de particulas
		*/
		std::vector<SharedParticleSystem> particleSystems;
		
	protected:

		/**
		* @brief ancho de la escena (en medidas para box2d)
		*/
		int sceneWidth;
		/**
		* @brief alto de la escena (en medidas para box2d)
		*/
		int sceneHeight;

		/**
		* @brief escala de box2D
		*/
		float scale;

	public:
		
		/**
		* @brief Constructor de una escena base 
		* @param gravity -> gravedad de la escena 
		* @param w -> ancho de la escena
		* @param h -> alto de la escena
		*/
		Scene(b2Vec2 gravity, int w, int h, float scale);

		/**
		* @brief limpiamos el mapa de objeto y el vector de sistemas de particulas
		*/
		virtual ~Scene();

	public:

		/**
		* @brief retorna el ancho de la escena
		*/
		inline int getSceneWidth() const
		{
			return sceneWidth;
		}

		/**
		* @brief retorna el alto de la escena
		*/
		inline int getSceneHeight() const
		{
			return sceneHeight;
		}

		/**
		* @brief la escala para pasar de unidades a pixeles, necesario para pasar de box2D a SFML
		*/
		inline float unitsToPixels() const
		{
			return scale;
		}

		/**
		* @brief la escana para pasar de pixeles a unidades, necesario para pasar de SFML a box2D
		*/
		inline float pixelsToUnits() const
		{
			return 1.f / scale;
		}

		/**
		* @brief retorna el mundo fisico de box2D
		*/
		b2World & getPhysicsWorld()
		{
			return physicsWorld;
		}

	public:

		/**
		* @brief a�ade un sistema de particulas 
		* @param position -> posicion del sistema de particulas
		* @param dir -> direccion de las particulas del sistema de particulas
		*/
		void addParticleSystem(sf::Vector2f position, sf::Vector2f dir);

		/**
		* @brief a�ade un objeto a la escena
		* @param name -> nombre del objeto 
		* @param gameObjecet -> el objeto
		*/
		void addGameObject(const String name, SharedGameObject & gameObject);

		/**
		* @brief retorna el objeto segun el nombre proporcionado
		* @param name -> nombre del objeto
		*/
		GameObject * getGameObject(const String & name);

	public:

		/**
		* @brief actualiza el juego
		* @param deltaTime -> tiempo entre frames, se le pasa desde la clase Game
		*/
		virtual void update(float deltaTime);

		/**
		* @brief actualiza el dibujado del juego
		* @param renderer -> RenderWindow de SFML
		*/
		virtual void render(sf::RenderWindow & renderer) = 0;

	};
}

#endif