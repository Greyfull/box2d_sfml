
/**
* @file Floor.hpp
* @author David Rogel Pernas
* @date 5/2/2018
* @class Floor
* @brief Clase que representa el suelo del juego
*/

#ifndef LUBRE_FLOOR_HEADER
#define LUBRE_FLOOR_HEADER

#include "GameObject.hpp"

namespace lubre
{
	class Floor : public GameObject
	{
	public:
		
		/**
		* @brief Constructor que llamar a todos los metodos necesarios para generar la estructura
		* @param scene -> escena padre
		*/
		Floor(Scene * scene);

	public:
		
		/**
		* @brief metodo que ejecuta las colisiones
		*/
		void onCollisionEnter() override;

	public:

		/**
		* @brief metodo que actualiza el gameObject
		* @param deltaTime -> tiempo entre frames, se pasa desde la clase Game
		*/
		void update(float deltaTime) override;
	};
}

#endif