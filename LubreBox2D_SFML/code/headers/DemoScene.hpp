
/**
* @file DemoScene.hpp
* @author David Rogel Pernas
* @date 1/12/2017
* @class DemoScene
* @brief Clase que hereda de Scene y ejecuta todos los objetos de la escena
*/

#ifndef LUBRE_DEMO_HEADER
#define LUBRE_DEMO_HEADER

#include "Scene.hpp"

namespace lubre
{
	class DemoScene : public Scene
	{
	public:

		/**
		* @brief Constructor donde se crean todos los objetos 
		* @param gravity -> gravedad del mundo
		* @param w -> ancho del mundo
		* @param h -> alto del mundo
		* @param scale -> escala del mundo de box2D, sevir� para pasar de sfml a box2D y vice versa 
		*/
		DemoScene(b2Vec2 gravity, int w, int h, float scale);

		~DemoScene()
		{
			particleSystems.clear();
		}

	public:

		/**
		* @brief actualiza los objetos
		* @param deltaTime -> tiempo entre frames, se le pasa desde la clase Game
		*/
		void update(float deltaTime) override;
		/**
		* @brief pinta los objetos
		* @param renderer -> ventana de render de SFML
		*/
		void render(sf::RenderWindow & renderer) override;
	};
}

#endif