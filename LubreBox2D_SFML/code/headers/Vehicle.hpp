
/**
* @file Vehicle.hpp
* @author David Rogel Pernas
* @date 12/1/2018
* @class Vehicle
* @brief Clase que representa el coche que maneja el jugador
*/

#ifndef LUBRE_VEHICLE_HEADER
#define LUBRE_VEHICLE_HEADER

#include "GameObject.hpp"

namespace lubre
{
	class Vehicle : public GameObject
	{
	public:

		/**
		* @brief Constructor que llamar a todos los metodos necesarios para generar la estructura
		* @param scene -> escena padre
		*/
		Vehicle(Scene * scene);

	public:
		
		/**
		* @brief metodo que ejecuta las colisiones
		*/
		void onCollisionEnter() override;

	public:

		/**
		* @brief metodo que actualiza el gameObject
		* @param deltaTime -> tiempo entre frames, se pasa desde la clase Game
		*/
		void update(float deltaTime) override;
	};
}

#endif