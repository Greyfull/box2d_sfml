
/**
* @file Meta.hpp
* @author David Rogel Pernas
* @date 5/2/2018
* @class Meta
* @brief Clase que representa la zona de meta del juego
* a donde el jugador tiene que llegar y depositar los objetos
*/

#ifndef LUBRE_META_HEADER
#define LUBRE_META_HEADER

#include "GameObject.hpp"

namespace lubre
{
	class Meta : public GameObject
	{
	private:

		/**
		* @brief indicador de si se ha acabado el juego
		*/
		bool isFinish;

	public:

		/**
		* @brief Constructor que llamar a todos los metodos necesarios para generar la estructura
		* @param scene -> escena padre
		*/
		Meta(Scene * scene);

	public:

		/**
		* @brief metodo que ejecuta las colisiones
		*/
		void onCollisionEnter() override;

	public:

		/**
		* @brief metodo que actualiza el gameObject
		* @param deltaTime -> tiempo entre frames, se pasa desde la clase Game
		*/
		void update(float deltaTime) override;
	};
}

#endif