
/**
* @file ParticleSystem.hpp
* @author David Rogel Pernas
* @date 7/2/2017
* @class CollisionListener
* @brief Clase que genera un sistema de particulas, generando un pool de particulas y actualizando cada una de ellas
*/

#ifndef LUBRE_PARTICLE_SYSTEM_HEADER
#define LUBRE_PARTICLE_SYSTEM_HEADER

#include <vector>
#include <memory>

#include "Particle.hpp"
#include <iostream>
using namespace std;

namespace lubre
{
	class ParticleSystem
	{
	private:

		typedef std::shared_ptr<Particle> SharedParticle;
		typedef sf::Vector2f vec2;

	private:

		/**
		* @brief Cantidad de particulas
		*/
		size_t count;
		/**
		* @brief posicion del sistema de particulas
		*/
		vec2 position;

		/**
		* @brief pool de particulas
		*/
		std::vector<SharedParticle> pool;

	public:

		/**
		* @brief Consctructor del sistema de particulas
		* @param dir -> direccion hacia donde va a ir la particula
		* @param count -> cantidad de particulas
		* @param position -> posicion del sistema de particula
		* @param color -> color de cada particula
		*/
		ParticleSystem(vec2 dir, size_t count, vec2 position, sf::Color color = sf::Color::White);
		
		/**
		* @brief Destructor del sistema de particulas, limpiamos el pool de particulas
		*/
		~ParticleSystem()
		{			
			pool.clear();
		}

	public:

		/**
		* @brief Actualizamos cada una de las particulas
		*/
		void update(float deltaTime);
		/**
		* @brief dibujamos acada una de las particulas
		*/
		void render(sf::RenderWindow & window);

	private:

		/**
		* @brief Creamos las particulas
		* @param dir -> direccion de la particula 
		* @param count -> cantidad de particulas
		* @param position -> posicion de las particulas
		* @param color -> color de las particulas
		*/
		void generateParticles(vec2 dir, size_t count, vec2 position, sf::Color color);
	};
}

#endif
