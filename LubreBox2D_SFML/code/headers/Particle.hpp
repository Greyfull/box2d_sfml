
/**
* @file Particle.hpp
* @author David Rogel Pernas
* @date 7/2/2018
* @class Particle
* @brief Clase para guardar los datos y actualizarlos de una Particula
*/


#ifndef LUBRE_PARTICLES_HEADER
#define LUBRE_PARTICLES_HEADER

#include <cstdint>
#include <SFML/Graphics.hpp>

namespace lubre
{
	class Particle
	{
	private:

		/**
		* @brief tipo de vector, si hubiera que cambiar a otro vector solo habr�a que cambiar ese y no toda la clase
		*/
		typedef sf::Vector2f vec2;
		
	private:

		/**
		* @brief Color de la particula
		*/
		sf::Color color;
		/**
		* @brief posicion de la particula
		*/
		vec2 position;
		/**
		* @brief velocidad de la particula
		*/
		vec2 velocity;

		/**
		* @brief cuanto se desvanece una particula
		*/
		float alphaSub;

		/**
		* @brief Ticks para desvanecer la particula o reducir un alfa
		*/
		uint32_t tick;

	public:

		/**
		* @brief Constructor de una particula
		* @param dirX -> direccion en el eje X
		* @param dirY -> direccion en el eje Y
		* @param position -> posicion de la particula
		* @param color -> color de la particula
		*/
		Particle(float dirX, float dirY, vec2 position, sf::Color color);
		~Particle() {}

	public:

		/**
		* @brief Obtenemos el color
		*/
		sf::Color getColor() const
		{
			return color;
		}

		/**
		* @brief Obtenemos la posicion
		*/
		vec2 getPosition() const
		{
			return position;
		}

		/**
		* @brief actualizamos la particula
		* @param deltaTime -> tiempo entre frames, se calcula en la Clase Game
		*/
		void update(float deltaTime);
		/**
		* @brief actualizamos el alfa de la particula
		*/
		bool poll();
	};
}

#endif