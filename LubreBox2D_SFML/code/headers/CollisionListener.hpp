
/**
* @file CollisionListener.hpp
* @author David Rogel Pernas
* @date 7/2/2017
* @class CollisionListener
* @brief Clase que lleva las colisiones entre objetos de box2d
*/

#ifndef LUBRE_COLLISION_LISTENER_HEADER
#define LUBRE_COLLISION_LISTENER_HEADER

#include <Box2D\Box2D.h>

namespace lubre
{
	class GameObject;

	class CollisionListener : public b2ContactListener
	{
	public:

		CollisionListener() {}

	public:

		/**
		* @brief metodo aportado por box2d que recive un contacto para poder manejarlo
		* @param contact -> punto de contacto entre dos bodies, se encarga box2d de llamar al metodo
		*/
		void BeginContact(b2Contact * contact) override;
	};
}

#endif