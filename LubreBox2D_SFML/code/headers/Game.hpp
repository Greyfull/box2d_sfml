
/**
* @file Game.hpp
* @author David Rogel Pernas
* @date 23/10/2017
* @class Game
* @brief Clase Game que crea una ventana de SFML y contiene el bucle principal de juego
*/

#ifndef LUBRE_GAME_HEADER
#define LUBRE_GAME_HEADER

#include <SFML\Graphics.hpp>

#include "Scene.hpp"

namespace lubre
{
	class Game
	{
	private:

		/**
		* @brief ventana de SFML
		*/
		sf::RenderWindow window;

		/**
		* @brief puntero a escena
		*/
		Scene * scene;

	public:

		/**
		* @brief Constructor de Game
		* @param title -> titulo de la ventana
		* @param width -> ancho de la ventana 
		* @param height -> alto de la ventana
		*/
		Game(const char* title, int width, int height);

	public:

		/**
		* @brief metodo para iniciar el Game
		*/
		void init();

	private:

		/**
		* @brief gameLoop del juego 
		* contiene el bucle principal
		*/
		void gameLoop();
		/**
		* @brief maneja los eventos de la Window
		*/
		void handleEvents();
		/**
		* @brief actualiza la escena
		* @param deltaTime -> tiempo entre frames
		*/
		void update(float deltaTime);
		/**
		* @brief Renderiza la escena
		*/
		void render();
	};
}

#endif
